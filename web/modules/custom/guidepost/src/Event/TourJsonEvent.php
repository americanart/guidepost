<?php

namespace Drupal\guidepost\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class TourJsonEvent
 * @package Drupal\guidepost\Event
 */
class TourJsonEvent extends Event {

  /**
   * The Tour or Tour Set object being acted on.
   *
   * @param \Drupal\guidepost\TourJson\Tour\Tour|\Drupal\guidepost\TourJson\TourSet $tourJson
   */
  protected $tourJson;

  /**
   * The Node being acted on.
   *
   * @param \Drupal\node\NodeInterface $node
   */
  protected $node;

  /**
   * TourJsonEvent constructor.
   * @param \Drupal\guidepost\TourJson\Tour\Tour|\Drupal\guidepost\TourJson\TourSet $tourJson
   * @param \Drupal\node\NodeInterface $node
   */
  public function __construct($tourJson, $node) {
    $this->tourJson = $tourJson;
    $this->node = $node;
  }

  /**
   * Return $tourJson
   * @param \Drupal\guidepost\TourJson\Tour\Tour|\Drupal\guidepost\TourJson\TourSet $tourJson
   */
  public function getTourJson() {
    return $this->tourJson;
  }

  /**
   * @return \Drupal\node\NodeInterface $ndoe
   */
  public function getNode() {
    return $this->node;
  }

}