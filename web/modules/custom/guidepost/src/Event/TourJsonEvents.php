<?php

namespace Drupal\guidepost\Event;

/**
 * Class TourJsonEvents
 * @package Drupal\guidepost
 */
final class TourJsonEvents {
  /**
   * Name of the event fired before a TourJson object is json encoded.
   * @Event
   * @var string
   */
  const TOUR_JSON = 'guidepost.tour_json';
}
