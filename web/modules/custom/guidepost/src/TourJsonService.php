<?php

namespace Drupal\guidepost;

use Drupal\Core\Config\ConfigFactoryInterface;
use \Drupal\guidepost\Event\TourJsonEvents;
use Drupal\guidepost\Event\TourJsonEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\file\Entity\File;
use Drupal\guidepost\TourJson\Asset\{Asset, AssetMetadata, Content, Rights, Source};
use Drupal\guidepost\TourJson\{Stop, TourSet};
use Drupal\guidepost\TourJson\Tour\{Connection, Metadata, Tour};
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Class TourJsonService.
 *  Convert a Drupal Node to Tour.json
 */
class TourJsonService implements TourJsonServiceInterface {

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Hidden content fields.
   *
   * @var array $hiddenFields
   */
  protected $hiddenFields = [];

  /**
   * Constructs a TourJsonService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   An event dispatcher instance to use for tour json events.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    $this->configFactory = $config_factory;
    $this->eventDispatcher = $event_dispatcher;
    $config = $this->configFactory->get('guidepost.settings');
    $this->hiddenFields = array_filter(array_values($config->get('hidden_optional_fields')));
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   */
  public function convert($node) {
    if ($node instanceof NodeInterface) {
      switch ($node->getType()) {
        case 'tour':
          return $this->convertTour($node);
          break;
        case 'tour_set':
          return $this->convertTourSet($node);
          break;
      }
    }
    return NULL;
  }

  /**
   * Convert a "Tour" Node into tour.json
   * @see http://schema.saam.media/v1/tour.schema.json
   * @param $node NodeInterface
   * @return string
   */
  private function convertTour($node) {
    /** @var \Drupal\guidepost\TourJson\Tour\Tour $tour */
    $tour = new Tour();
    $tour->setId($node->uuid());
    // Set Stops.
    // TODO: Test if the stop IDs that make up connections exist in the Tour stops?
    $stops = [];
    $stopNodes = $node->field_stops->referencedEntities();
    foreach ($stopNodes as $stopNode) {
      $stop = $this->convertStop($stopNode);
      $stops[] = $stop;
    }
    $tour->setStops($stops);
    $this->filterEmptyProperty($tour, 'stops');
    // Set Metadata.
    $metadata = new Metadata();
    $metadata->setTitle($node->getTitle());
    $this->filterEmptyProperty($metadata, 'title');
    // Set the Description
    $metadata->setDescription($node->get('field_description')->value);
    $this->filterEmptyProperty($metadata, 'description');
    // Set the (optional) Language
    if (!in_array('field_optional_language', $this->hiddenFields)
      && $langcode = $node->get('field_optional_language')->value) {
      $metadata->{"language"} = $langcode;
    }
    $authors = array_column($node->get('field_authors')->getValue(), 'value');
    if ($authors) {
      $metadata->setAuthors($authors);
    }
    $this->filterEmptyProperty($metadata, 'authors');
    // Set Root stop.
    $rootStopNode = $node->field_root_stop->entity;
    if ($rootStopNode instanceof NodeInterface) {
      $rootStop = $this->convertStop($rootStopNode);
      $metadata->setRootStop($rootStop);
    }
    $this->filterEmptyProperty($metadata, 'rootStop');
    // Set Resources.
    $assets = [];
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    foreach($node->field_resources->referencedEntities() as $paragraph) {
      $asset = $this->convertAsset($paragraph);
      $assets[] = $asset;
    }
    $metadata->setResources($assets);
    $this->filterEmptyProperty($metadata, 'resources');
    // Set Connections.
    $connections = [];
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    foreach($node->field_connections->referencedEntities() as $paragraph) {
      $connection = $this->convertConnection($paragraph);
      $connections[] = $connection;
    }
    $metadata->setConnections($connections);
    $this->filterEmptyProperty($metadata, 'connections');

    $tour->setMetadata($metadata);
    $this->filterEmptyProperty($tour, 'metadata');
    // Dispatch event.
    $event = new TourJsonEvent($tour, $node);
    $this->eventDispatcher->dispatch(TourJsonEvents::TOUR_JSON, $event);

    return $tour->toJson();
  }

  /**
   * Convert a "Tour" Node into a URI to the tour.json endpoint
   * @see http://schema.saam.media/v1/tour.schema.json
   * @param $node NodeInterface
   * @return string
   */
  private function convertTourToUri($node) {
    return sprintf('%s/%s/%s', \Drupal::request()->getSchemeAndHttpHost(), 'api/v1/tours', $node->id());
  }

  /**
   * Convert a "Tour Set" Node into tour-set.json
   * @see http://schema.saam.media/v1/tour-set.schema.json
   * @param $node NodeInterface
   * @return string
   */
  private function convertTourSet($node) {
    /** @var \Drupal\guidepost\TourJson\TourSet $tourSet */
    $tourSet = new TourSet();
    $tourSet->setId($node->uuid());
    $useUriRefs = $node->get('field_use_uri_references')->value == 1 ? TRUE : FALSE;
    $tours = [];
    /** @var \Drupal\node\NodeInterface $tourNode */
    foreach($node->field_tours->referencedEntities() as $tourNode) {
      if ($useUriRefs) {
        $tour = $this->convertTourToUri($tourNode);
      }
      else {
        $tour = json_decode($this->convertTour($tourNode));
      }
      if ($tour) {
        $tours[] = $tour;
      }
    }
    $tourSet->setTours($tours);
    $this->filterEmptyProperty($tourSet, 'tours');
    // Dispatch event.
    $event = new TourJsonEvent($tourSet, $node);
    $this->eventDispatcher->dispatch(TourJsonEvents::TOUR_JSON, $event);

    return $tourSet->toJson();
  }

  /**
   * Convert a "Stop" Node into an object
   * @see http://schema.saam.media/v1/tour.schema.json#/definitions/stop
   * @param $node NodeInterface
   * @return \Drupal\guidepost\TourJson\Stop
   */
  private function convertStop($node) {
    $stop = new Stop();
    $stop->setId($node->uuid());
    // Set the "Stop Type" property.
    $term = $node->field_stop_type->entity;
    if ($term instanceof TermInterface) {
      $stop->setType($term->getName());
    }
    $this->filterEmptyProperty($stop, 'type');
    // Set the Description.
    $stop->setDescription($node->get('field_description')->value);
    $this->filterEmptyProperty($stop, 'description');
    // Set the (optional) Stop Number
    if (!in_array('field_optional_stop_number', $this->hiddenFields)
      && $number = $node->get('field_optional_stop_number')->value) {
      $stop->{"stopNumber"} = $number;
    }
    // Set Assets.
    $assets = [];
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    foreach($node->field_assets->referencedEntities() as $paragraph) {
      $asset = $this->convertAsset($paragraph);
      $assets[] = $asset;
    }
    $stop->setAssets($assets);
    $this->filterEmptyProperty($stop, 'assets');
    // Convert the Geo field data to a GeoJSON formatted object.
    $geoFieldValue = $node->get('field_geo')->value;
    if ($geoFieldValue) {
      $geo = \geoPHP::load($geoFieldValue, 'wkt');
      $geoJson = json_decode($geo->out('geojson'));
      if ($geoJson) {
        $stop->setGeo($geoJson);
      }
    }
    $this->filterEmptyProperty($stop, 'geo');
    return $stop;
  }

  /**
   * Convert an "Asset" Paragraph and referenced Media into an object
   * @see http://schema.saam.media/v1/tour.schema.json#/definitions/asset
   * @param $paragraph \Drupal\paragraphs\ParagraphInterface
   * @return \Drupal\guidepost\TourJson\Asset\Asset
   */
  private function convertAsset($paragraph) {
    $asset = new Asset();
    $asset->setId($paragraph->uuid());
    // Set Usage property.
    $term = $paragraph->field_usage->entity;
    if ($term instanceof TermInterface) {
      $asset->setUsage($term->getName());
    }
    $this->filterEmptyProperty($asset, 'usage');
    // Handle the referenced "Rights" paragraph.
    if ($rightsParagraph = $paragraph->field_rights->entity) {
      $rights = $this->buildRights($rightsParagraph);
      $asset->setRights($rights);
    }
    $this->filterEmptyProperty($asset, 'rights');
    // Get Asset "source" or "content" objects.
    if ($paragraph->hasField('field_content')) {
      $content = $this->buildAssetContent($paragraph);
      $asset->setContent($content);
      unset($asset->source);
    }
    elseif ($paragraph->hasField('field_source')) {
      $source = $this->buildAssetSource($paragraph);
      $asset->setSource($source);
      unset($asset->content);
    }

    return $asset;
  }

  /**
   * Instantiate the content property of an Asset.
   * @param ParagraphInterface $paragraph
   * @return \Drupal\guidepost\TourJson\Asset\Content
   */
  public function buildAssetContent($paragraph) {
    $content = new Content();
    $assetMetadata = new AssetMetadata();
    $data = $paragraph->get('field_content')->value;
    $content->setData($data);
    $term = $paragraph->field_part->entity;
    if ($term instanceof TermInterface) {
      $part = $term->getName();
      $assetMetadata->setPart($part);
    }
    $this->filterEmptyProperty($assetMetadata, 'part');
    // Get the format from the field text formats.
    $fieldFormat = $paragraph->get('field_content')->format;
    switch ($fieldFormat) {
      case 'plain_text':
        $format = 'text/plain';
        break;
      case 'full_html':
        $format = 'text/html';
        break;
      default:
        $format = $fieldFormat;
    }
    $assetMetadata->setFormat($format);
    $this->filterEmptyProperty($assetMetadata, 'format');
    // TODO: How do we get the LastModified metadata from a "text" paragraph?
    $this->filterEmptyProperty($assetMetadata, 'lastModified');
    $content->setMetadata($assetMetadata);
    $this->filterEmptyProperty($content, 'metadata');
    return $content;
  }

  /**
   * Instantiate the source property of an Asset.
   * @param ParagraphInterface $paragraph
   * @return \Drupal\guidepost\TourJson\Asset\Source
   */
  public function buildAssetSource($paragraph) {
    $source = new Source();
    $assetMetadata = new AssetMetadata();
    $term = $paragraph->field_part->entity;
    if ($term instanceof TermInterface) {
      $part = $term->getName();
      $assetMetadata->setPart($part);
    }
    $this->filterEmptyProperty($assetMetadata, 'part');
    // Get the media entity.
    /** @var \Drupal\media\MediaInterface $media */
    $media = $paragraph->field_source->entity;
    $file = null;
    if ($media instanceof MediaInterface) {
      $format = $media->get('field_mime_type')->value;
      $assetMetadata->setFormat($format);
      $this->filterEmptyProperty($assetMetadata, 'format');
      $changedDate = date('Y-m-d H:i:s', $media->getChangedTime());
      $lastModified = $this->getFormattedDateTime($changedDate);
      $assetMetadata->setLastModified($lastModified);
      $this->filterEmptyProperty($assetMetadata, 'lastModified');
      switch ($media->bundle()) {
        case 'audio':
          $file = $media->get('field_media_audio_file')->entity;
          // Set the (optional) Speaker
          if (!in_array('field_optional_speaker', $this->hiddenFields)
            && $speaker = $paragraph->get('field_optional_speaker')->value) {
            $assetMetadata->{"speaker"} = $speaker;
          }
          break;
        case 'file':
          $file = $media->get('field_media_file')->entity;
          break;
        case 'image':
          $file = $media->get('field_media_image')->entity;
          break;
        case 'remote_video':
          $provider = $media->get('field_provider')->value;
          $assetMetadata->setFormat(strtolower($provider) . '/video');
          $uri = $media->get('field_media_oembed_video')->value;
          $source->setUri($uri);
          break;
        case 'video':
          $file = $media->get('field_media_video_file')->entity;
          break;
        default:
          $file = null;
      }
      if ($file instanceof File) {
        // $uri = Url::fromUri($path,['absolute' => TRUE])->toString();
        $source->setUri($file->url());
      }
    }
    $this->filterEmptyProperty($source, 'uri');
    $source->setMetadata($assetMetadata);
    $this->filterEmptyProperty($source, 'metadata');
    return $source;
  }

  /**
   * Instantiate the rights property of an Asset.
   * @param ParagraphInterface $paragraph
   * @return \Drupal\guidepost\TourJson\Asset\Rights
   */
  public function buildRights($paragraph) {
    $rights = new Rights();
    if ($paragraph instanceof ParagraphInterface && $paragraph->getType() == 'rights') {
      if ($copyright = $paragraph->get('field_copyright')->value) {
        $rights->setCopyright($copyright);
      }
      if ($credit = $paragraph->get('field_credit_line')->value) {
        $rights->setCreditLine($credit);
      }
      if ($expires = $paragraph->get('field_expiration')->value) {
        $expirationDate = $this->getFormattedDateTime($expires);
        $rights->setExpiration($expirationDate);
      }
      // Filter empty properties.
      $this->filterEmptyProperty($rights, 'copyright');
      $this->filterEmptyProperty($rights, 'creditLine');
      $this->filterEmptyProperty($rights, 'expiration');
    }
    return $rights;
  }

  /**
   * Convert a "Connection" Paragraph and into an object
   * @see http://schema.saam.media/v1/tour.schema.json#/definitions/connection
   * @param $paragraph \Drupal\paragraphs\ParagraphInterface
   * @return \Drupal\guidepost\TourJson\Tour\Connection
   */
  private function convertConnection($paragraph) {
    $connection = new Connection();
    /** @var NodeInterface $sourceStop */
    $sourceStop = $paragraph->field_source_stop->entity;
    if ($sourceStop) {
      $connection->setSourceId($sourceStop->uuid());
    }
    /** @var NodeInterface $destinationStop */
    $destinationStop = $paragraph->field_destination_stop->entity;
    if ($destinationStop) {
      $connection->setDestinationId($destinationStop->uuid());
    }
    $term = $paragraph->field_usage->entity;
    if ($term instanceof TermInterface) {
      $connection->setUsage($term->getName());
    }
    $priority = $paragraph->field_priority->value;
    if ($priority) {
      $connection->setPriority($priority);
    }
    // Filter empty properties.
    $this->filterEmptyProperty($connection, 'sourceId');
    $this->filterEmptyProperty($connection, 'destinationId');
    $this->filterEmptyProperty($connection, 'usage');
    $this->filterEmptyProperty($connection, 'priority');
    return $connection;
  }

  /**
   * Test property value and unset if empty/null
   * @param object $object
   *   The object to check.
   * @param string $prop
   *   The property name
   */
  public function filterEmptyProperty(&$object, $prop) {
    // TODO: Find a better way to walk through a complete object and remove empty.
    if (property_exists($object, $prop) && empty($object->{$prop})) {
      unset($object->{$prop});
    }
  }

  /**
   * Get a Drupal formatted date string.
   *
   * @param string $dateString
   * @return string
   */
  public function getFormattedDateTime($dateString) {
    if ($dateString) {
      $date = new DrupalDateTime($dateString);
      $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      return \Drupal::service('date.formatter')->format($date->getTimestamp(), 'custom', DateTimeItemInterface::DATETIME_STORAGE_FORMAT, DateTimeItemInterface::STORAGE_TIMEZONE);
    }
    else {
      return null;
    }
  }


}
