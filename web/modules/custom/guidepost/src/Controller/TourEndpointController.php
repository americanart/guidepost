<?php

namespace Drupal\guidepost\Controller;

use Drupal\guidepost\Controller\ApiBaseController;
use Drupal\guidepost\TourJson\Tour\Tour;
use Drupal\node\NodeInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TourEndpointController.
 */
class TourEndpointController extends ApiBaseController {

  /**
   * Get JSON response for a Tour.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return CacheableJsonResponse
   */
  public function get(NodeInterface $node, Request $request) {
    $response = new CacheableJsonResponse();
    $cache_metadata = $this->getCacheableMetadata();
    try {
      if ($node instanceof NodeInterface) {
        $response->setContent($this->tourJsonService->convert($node));
        $response->setStatusCode(200);
        // Add cache dependency on Node ID
        $cache_dependencies = ['node:' . $node->id()];
        $cache_metadata->setCacheTags($cache_dependencies);
        $response->addCacheableDependency($cache_metadata);
      }
    }
    catch (RequestException $exception) {
      $this->loggerFactory->get('guidepost')->error($exception);
      $response->setContent(\GuzzleHttp\json_encode([
        'code' => 500,
        'message' => $exception->getMessage(),
      ]));
      $response->setStatusCode(500);
    }
    return $response;
  }

}
