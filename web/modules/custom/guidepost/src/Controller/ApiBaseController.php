<?php

namespace Drupal\guidepost\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\guidepost\TourJsonServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Class ApiBaseController.
 */
class ApiBaseController extends ControllerBase {

  /**
   * Symfony\Component\DependencyInjection\ContainerAwareInterface definition.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerAwareInterface
   */
  protected $entityQuery;
  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;
  /**
   * Drupal\guidepost\TourJsonServiceInterface definition.
   *
   * @var \Drupal\guidepost\TourJsonServiceInterface
   */
  protected $tourJsonService;

  /**
   * Constructs a new TourEndpointController object.
   */
  public function __construct(ContainerAwareInterface $entity_query,
                              LoggerChannelFactoryInterface $logger_factory,
                              TourJsonServiceInterface $tour_json_service) {
    $this->entityQuery = $entity_query;
    $this->loggerFactory = $logger_factory;
    $this->tourJsonService = $tour_json_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query'),
      $container->get('logger.factory'),
      $container->get('guidepost.tour_json')
    );
  }

  /**
   * Create a Cacheable Metadata Object.
   *
   * @return CacheableMetadata
   */
  public function getCacheableMetadata() {
    // Create cache metadata.
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheMaxAge(86400);
    return $cache_metadata;
  }

}
