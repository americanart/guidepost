<?php

namespace Drupal\guidepost\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ExampleTourJsonSubscriber.
 *   An example demonstrating how to modify tour.json output by subscribing to
 *   a dispatched event.
 */
class ExampleTourJsonSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events['guidepost.tour_json'] = ['guidepost_tour_json'];

    return $events;
  }

  /**
   * This method is called whenever the guidepost.tour_json event is dispatched
   *   and allows modifications to a TourJson "Tour" or "TourSet" before JSON
   *   encoding.
   *
   * @param \Drupal\guidepost\Event\TourJsonEvent $event
   */
  public function guidepost_tour_json(Event $event) {}

}
