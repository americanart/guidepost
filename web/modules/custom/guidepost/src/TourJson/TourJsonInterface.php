<?php

namespace Drupal\guidepost\TourJson;

interface TourJsonInterface {

  /**
   * @return string
   */
  public function getId();
  /**
   * @return string
   */
  public function toJson();

}