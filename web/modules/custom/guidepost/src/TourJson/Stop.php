<?php

namespace Drupal\guidepost\TourJson;

/**
 * Class Stop
 *   A tour.json stop. Stops represent the nodes of the tour graph.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json#/definitions/stop
 */

class Stop {

  /**
   * An unique ID.
   * @var string $id
   */
  public $id;

  /**
   * An identifier for the type of Stop.
   * @var string $type
   */
  public $type;

  /**
   * The Tour Description.
   * @var string
   */
  public $description;

  /**
   * The GeoJSON data.
   *   see: https://geojson.org/schema/GeoJSON.json
   * @var object $geo
   */
  public $geo;

  /**
   * A set of stop assets.
   * @var \Drupal\guidepost\TourJson\Asset\Asset[]
   */
  public $assets= [];

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Asset\Asset[]
   */
  public function getAssets() {
    return $this->assets;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Asset\Asset[] $assets
   */
  public function setAssets($assets) {
    $this->assets = $assets;
  }

  /**
   * @return object
   */
  public function getGeo() {
    return $this->geo;
  }

  /**
   * @param object $geo
   */
  public function setGeo($geo) {
    $this->geo = $geo;
  }

}