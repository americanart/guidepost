<?php

namespace Drupal\guidepost\TourJson\Tour;

/**
 * Class Connection
 *   A tour.json connection between two stops.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json#/definitions/connection
 */

class Connection {

  /**
   * The Source ID.
   *   The ID of the starting node in a connection.
   * @var string $usage
   */
  public $sourceId;

  /**
   * The Destination ID.
   *   The ID of the ending node in a connection.
   * @var string $usage
   */
  public $destinationId;

  /**
   * The Connection usage.
   *   An indicator on how to use the connection in a client application.
   * @var string $usage
   */
  public $usage;

  /**
   * The Connection priority.
   *   A number used to weight connections.
   * @var int $priority
   */
  public $priority;

  /**
   * @return string
   */
  public function getSourceId() {
    return $this->sourceId;
  }

  /**
   * @param string $sourceId
   */
  public function setSourceId($sourceId) {
    $this->sourceId = $sourceId;
  }

  /**
   * @return string
   */
  public function getDestinationId() {
    return $this->destinationId;
  }

  /**
   * @param string $destinationId
   */
  public function setDestinationId($destinationId) {
    $this->destinationId = $destinationId;
  }

  /**
   * @return string
   */
  public function getUsage() {
    return $this->usage;
  }

  /**
   * @param string $usage
   */
  public function setUsage($usage) {
    $this->usage = $usage;
  }

  /**
   * @return int
   */
  public function getPriority() {
    return $this->priority;
  }

  /**
   * @param int $priority
   */
  public function setPriority($priority) {
    $this->priority = $priority;
  }

}