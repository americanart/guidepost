<?php

namespace Drupal\guidepost\TourJson\Tour;

/**
 * Class Metadata
 *   The TourJson Tour Metadata.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json
 */

class Metadata {

  /**
   * The Tour Title.
   * @var string
   */
  public $title;

  /**
   * The Tour Description.
   * @var string
   */
  public $description;

  /**
   * The Authors.
   * @var array
   */
  public $authors = [];

  /**
   * The root stop for the tour.
   * @var \Drupal\guidepost\TourJson\Stop $rootStop
   */
  public $rootStop;

  /**
   * Tour assets and their usage.
   * @var \Drupal\guidepost\TourJson\Asset\Asset[]
   */
  public $resources = [];

  /**
   * Tour Stop Connections.
   * @var \Drupal\guidepost\TourJson\Tour\Connection[]
   */
  public $connections = [];

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param string $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return array
   */
  public function getAuthors() {
    return $this->authors;
  }

  /**
   * @param array $authors
   */
  public function setAuthors($authors) {
    $this->authors = $authors;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Stop
   */
  public function getRootStop() {
    return $this->rootStop;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Stop $stop
   */
  public function setRootStop($stop) {
    $this->rootStop = $stop;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Asset\Asset[]
   */
  public function getResources() {
    return $this->resources;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Asset\Asset[] $resources
   */
  public function setResources($resources) {
    $this->resources = $resources;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Tour\Connection[]
   */
  public function getConnections() {
    return $this->connections;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Tour\Connection[] $connections
   */
  public function setConnections($connections) {
    $this->connections = $connections;
  }


}