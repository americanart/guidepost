<?php

namespace Drupal\guidepost\TourJson\Tour;

use Drupal\guidepost\TourJson\TourJsonBase;
use Drupal\guidepost\TourJson\TourJsonInterface;

/**
 * Class Tour
 *   The TourJson Tour.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json
 */

class Tour extends TourJsonBase implements TourJsonInterface {

  /**
   * The Tour metadata.
   * @var \Drupal\guidepost\TourJson\Tour\Metadata
   */
  public $metadata;

  /**
   * A set of tour stops.
   * @var \Drupal\guidepost\TourJson\Stop[]
   */
  public $stops = [];

  /**
   * Get the current object as a JSON string.
   * @return string|bool
   */
  public function toJson() {
    return json_encode($this, JSON_NUMERIC_CHECK);
  }

  /**
   * @return \Drupal\guidepost\TourJson\Tour\Metadata
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Tour\Metadata $metadata
   */
  public function setMetadata($metadata) {
    $this->metadata = $metadata;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Stop[]
   */
  public function getStops() {
    return $this->stops;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Stop[] $stops
   */
  public function setStops($stops) {
    $this->stops = $stops;
  }

}