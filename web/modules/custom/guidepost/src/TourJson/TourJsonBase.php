<?php

namespace Drupal\guidepost\TourJson;

/**
 * Class BaseType
 *   The TourJson BaseType.
 *
 * @see https://gitlab.com/americanart/tour-json
 */

class TourJsonBase {

  /**
   * The unique ID.
   * @var string
   */
  public $id;

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

}