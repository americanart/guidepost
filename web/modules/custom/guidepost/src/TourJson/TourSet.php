<?php

namespace Drupal\guidepost\TourJson;

/**
 * Class TourSet
 *   The TourJson Tour Set.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour-set.schema.json
 */

class TourSet extends TourJsonBase implements TourJsonInterface {

  /**
   * A set of Tours.
   * @var \Drupal\guidepost\TourJson\Tour\Tour[]|array
   */
  public $tours = [];

  /**
   * Get the current object as a JSON string.
   *
   * @return string|bool
   */
  public function toJson() {
    return json_encode($this, JSON_NUMERIC_CHECK);
  }

  /**
   * @return array|\Drupal\guidepost\TourJson\Tour\Tour[]
   */
  public function getTours() {
    return $this->tours;
  }

  /**
   * @param array|\Drupal\guidepost\TourJson\Tour\Tour[] $tours
   */
  public function setTours($tours) {
    $this->tours = $tours;
  }

}