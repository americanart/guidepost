<?php

namespace Drupal\guidepost\TourJson\Asset;

/**
 * Class Asset
 *   A tour.json media asset. Assets are components used to build a Stop.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json#/definitions/asset
 */

class Asset {

  /**
   * An unique ID.
   * @var string $id
   */
  public $id;

  /**
   * The Asset usage.
   *   An indicator on how to use the asset in a client application.
   * @var string $usage
   */
  public $usage;

  /**
   * The Asset rights.
   * @var \Drupal\guidepost\TourJson\Asset\Rights
   */
  public $rights;

  /**
   * The Asset content.
   * @var \Drupal\guidepost\TourJson\Asset\Content
   */
  public $content;

  /**
   * The Asset source.
   * @var \Drupal\guidepost\TourJson\Asset\Source
   */
  public $source;

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getUsage() {
    return $this->usage;
  }

  /**
   * @param string $usage
   */
  public function setUsage($usage) {
    $this->usage = $usage;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Asset\Rights
   */
  public function getRights() {
    return $this->rights;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Asset\Rights $rights
   */
  public function setRights(Rights $rights) {
    $this->rights = $rights;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Asset\Content
   */
  public function getContent() {
    return $this->content;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Asset\Content $content
   */
  public function setContent(Content $content) {
    $this->content = $content;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Asset\Source
   */
  public function getSource() {
    return $this->source;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Asset\Source $source
   */
  public function setSource(Source $source) {
    $this->source = $source;
  }

}