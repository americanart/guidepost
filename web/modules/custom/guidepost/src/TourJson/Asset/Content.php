<?php

namespace Drupal\guidepost\TourJson\Asset;

/**
 * Class Content
 *   A tour.json asset content object.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json#/definitions/asset
 */

class Content {

  /**
   * The media asset data.
   * @var string $uri
   */
  public $data;

  /**
   * The Asset content metadata.
   * @var \Drupal\guidepost\TourJson\Asset\AssetMetadata
   */
  public $metadata;

  /**
   * @return string
   */
  public function getData() {
    return $this->data;
  }

  /**
   * @param string $data
   */
  public function setData($data) {
    $this->data = $data;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Asset\AssetMetadata
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Asset\AssetMetadata $metadata
   */
  public function setMetadata($metadata) {
    $this->metadata = $metadata;
  }

}