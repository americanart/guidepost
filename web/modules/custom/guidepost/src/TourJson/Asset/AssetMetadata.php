<?php

namespace Drupal\guidepost\TourJson\Asset;

/**
 * Class Metadata
 *   The TourJson Asset Metadata.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json#/definitions/assetMetadata
 */

class AssetMetadata {

  /**
   * The MIME Type of an asset.
   * @var string
   */
  public $format;

  /**
   * An identifier for the part a particular asset source or content object represents related to siblings.
   * @var string
   */
  public $part;

  /**
   * The modified date.
   * @var string
   */
  public $lastModified;

  /**
   * @return string
   */
  public function getFormat() {
    return $this->format;
  }

  /**
   * @param string $format
   */
  public function setFormat($format) {
    $this->format = $format;
  }

  /**
   * @return string
   */
  public function getPart() {
    return $this->part;
  }

  /**
   * @param string $part
   */
  public function setPart($part) {
    $this->part = $part;
  }

  /**
   * @return string
   */
  public function getLastModified() {
    return $this->lastModified;
  }

  /**
   * @param string $lastModified
   */
  public function setLastModified($lastModified) {
    $this->lastModified = $lastModified;
  }

}