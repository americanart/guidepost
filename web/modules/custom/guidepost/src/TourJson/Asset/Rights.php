<?php

namespace Drupal\guidepost\TourJson\Asset;

/**
 * Class Rights
 *   A tour.json asset rights. (Copyright Information and/or attribution).
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json#/definitions/rights
 */

class Rights {

  /**
   * The asset Copyright.
   *   A display string for a copyright.
   * @var string
   */
  public $copyright;

  /**
   * The asset credit line.
   * @var string
   */
  public $creditLine;

  /**
   * The asset right expiration.
   *   When the copyright expires.
   * @var string
   */
  public $expiration;

  /**
   * @return string
   */
  public function getCopyright() {
    return $this->copyright;
  }

  /**
   * @param string $copyright
   */
  public function setCopyright($copyright) {
    $this->copyright = $copyright;
  }

  /**
   * @return string
   */
  public function getCreditLine() {
    return $this->creditLine;
  }

  /**
   * @param string $creditLine
   */
  public function setCreditLine($creditLine) {
    $this->creditLine = $creditLine;
  }

  /**
   * @return string
   */
  public function getExpiration() {
    return $this->expiration;
  }

  /**
   * @param string $expiration
   */
  public function setExpiration($expiration) {
    $this->expiration = $expiration;
  }

}