<?php

namespace Drupal\guidepost\TourJson\Asset;

/**
 * Class Source
 *   A tour.json asset source object.
 *
 * @see https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json#/definitions/asset
 */

class Source {

  /**
   * The media asset URI.
   * @var string $uri
   */
  public $uri;

  /**
   * The Asset content metadata.
   * @var \Drupal\guidepost\TourJson\Asset\AssetMetadata
   */
  public $metadata;

  /**
   * @return string
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * @param string $uri
   */
  public function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * @return \Drupal\guidepost\TourJson\Asset\AssetMetadata
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * @param \Drupal\guidepost\TourJson\Asset\AssetMetadata $metadata
   */
  public function setMetadata($metadata) {
    $this->metadata = $metadata;
  }

}