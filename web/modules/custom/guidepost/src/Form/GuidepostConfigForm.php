<?php

namespace Drupal\guidepost\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GuidepostConfigForm.
 *   Global settings for the Guidepost distribution.
 */
class GuidepostConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'guidepost.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'guidepost_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('guidepost.settings');

    // Show Optional Fields checkboxes.
    $form['hidden_optional_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Hide the following optional fields on content forms'),
      '#description' => $this->t('Guidepost provides several optional content 
      fields which are commonly used in applications but not defined in tour.schema.json. 
      This setting allows you to remove the optional fields from Content forms and JSON output.'),
      '#options' => [
        'field_optional_language' => $this->t('Tour: Language'),
        'field_optional_stop_number' => $this->t('Stop: Stop Number'),
        'field_optional_speaker' => $this->t('Audio Asset: Speaker'),
      ],
      '#default_value' => $config->get('hidden_optional_fields'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('guidepost.settings')
      ->set('hidden_optional_fields', $form_state->getValue('hidden_optional_fields'))
      ->save();
  }

}
