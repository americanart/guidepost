<?php

namespace Drupal\guidepost;

interface TourJsonServiceInterface {

  /**
   * @param $data mixed
   * @return string
   */
  public function convert($data);

}