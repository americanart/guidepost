## Documentation
[Read the documentation](https://gitlab.com/americanart/guidepost/wikis/Documentation)

## Installation

### Requirements
Assuming an environment meeting Drupal 8's [system requirements](https://www.drupal.org/docs/8/system-requirements):
- PHP 7.1 or higher
- MySQL 5.5.3/MariaDB 5.5.20 or higher
- Apache/Nginx

Requires [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx) and [Git](https://git-scm.com/)

### Install
1. Create the project:
```
composer create-project americanart/guidepost:dev-8.x-1.x guidepost --no-interaction
```
2. Create or edit the .env file in the project directory with your database credentials. (see the `.env.example` file):
```
cd guidepost
cp .env.example .env
```
3. Install the site from configuration using Drush:
```
cd web
../vendor/bin/drush site:install --existing-config --account-name=admin --account-pass=admin
```

## Updating
Guidepost uses the [Configuration Synchronizer](https://www.drupal.org/project/config_sync) module to help updating existing installations and merge configuration changes when customizations have been made to the Distribution.
1. Update the project
```
composer update americanart/guidepost
```
2. Merge potential configuration changes from the distribution into your project:
```
cd web
../vendor/bin/drush config-distro-update
```

## Local development
While not a requirement, project maintainers recommend [Lando](https://docs.devwithlando.io/) for a simple local development environment.
To get started, install [Docker](https://www.docker.com/products/container-runtime) and [Lando](https://github.com/lando/lando/releases) locally.

1. Create the project:
```
composer create-project americanart/guidepost:dev-8.x-1.x guidepost --no-interaction
```
2. Create or edit the .env file in the project directory with your database credentials. (the `.env.example` file uses the Lando defaults):
```
cd guidepost
cp .env.example .env
```
3. Start Lando from the project root. This creates all the necessary Docker containers as "services".
```
lando start
```
4. Install Drupal from Guidepost's existing configuration
```
lando drush site:install --existing-config --account-name=admin --account-pass=admin
```
